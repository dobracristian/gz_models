# gz_models #

Various gazebo models from the [IAI Group Uni Bremen](http://ai.uni-bremen.de/)

# Usage #

Add this folder to your gazebo model paths:
~~~
echo "export GAZEBO_MODEL_PATH=/<path>/gz_models:${GAZEBO_MODEL_PATH}" >> ~/.bashrc
source ~/.bashrc
~~~